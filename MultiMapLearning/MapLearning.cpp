﻿// MapLearning.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <string>
#include <map>

/// @brief 插件配置项定义
struct MyConfig
{
	/// @brief 配置项值
	std::string Value;
	/// @brief 配置项是否只读，默认为只读
	bool ReadOnly = true;
};

void test01() {
	std::map<std::string, std::string> authors = {
		{"Joyce", "James"},
		{"Austen", "Jane"},
		{"Dickens", "Charles"}
	};

	for (auto& n : authors)
		std::cout << "first is " << n.first << ", second is " << n.second << std::endl;
}

void test02() {
	std::string name;
	std::map<std::string, MyConfig> kv = {
		{"name", {"hrl", true}},
		{"int", {"100", true}},
		{"double", {"0.5", true}}
	};

	for (auto& n : kv)
		std::cout << "first is " << n.first << ", second is " << n.second.Value << std::endl;

	std::cin >> name;
	std::cin.get();

	std::cout << "the Value is: " << kv[name].Value << std::endl;
	std::cout << "the ReadOnly is: " << kv[name].ReadOnly << std::endl;

	if (name == "int")
	{
		int value = std::stoi(kv[name].Value);
		std::cout << value << std::endl;
	}
}

int main()
{
	test01();

	std::cout << std::endl;

	test02();
	return 0;
}
