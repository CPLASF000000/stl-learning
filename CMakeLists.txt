# 设置Cmake 的最小版本
cmake_minimum_required(VERSION 3.13)

# 项目名字即：文件夹的名字
project(STL-Learning)

add_subdirectory("BitsetsLearning")

add_subdirectory("DequeLearning")

add_subdirectory("IteratorLearning")

#add_subdirectory("Mainconsole")

add_subdirectory("MapLearning")

#add_subdirectory("PairLearning")

add_subdirectory("SetLearning")

#add_subdirectory("STL-Learning")

#add_subdirectory("StringLearning")

add_subdirectory("VectorLearning")

add_subdirectory("StackLearning")
