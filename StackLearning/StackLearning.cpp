﻿// StackLearning.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <string>
#include <stack>

/// @brief 如何获取栈顶元素
void testStack_top() {

	std::stack<int> stack_;

	for (int i = 0; i < 10; i++)
	{
		stack_.push(i);
	}
	
	std::cout << "栈顶元素为：" << stack_.top() << std::endl;
}

/// @brief 如何获取栈顶元素
void testStack_top2() {

	std::stack<int> stack_;

	for (int i = 0; i < 10; i++)
	{
		stack_.push(i);
	}

	int top = stack_.top();
	stack_.pop();
	std::cout << "栈顶次元素为：" << stack_.top() << std::endl;
	stack_.push(top);
}

int main()
{
	std::cout << "started." << std::endl;
	testStack_top();

	testStack_top2();

	return 0;
}
