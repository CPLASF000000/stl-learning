#include <iostream>
#include <string>
#include <vector>

using namespace std;
using std::vector;

int main() {
    vector<int> ivec;

    // 无法赋值
    //vector<string> svec = ivec;

    vector<string> svec1(10, "null");
    return 0;
}