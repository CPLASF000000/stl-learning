# STL-Learning

这个仓库是用来学习cpp的STL库但又不仅仅是STL库，或者说应该是学习cpp里所有的容器的。

cpp的STL库有哪些内容呢？（~~cpp里到底有多少容器呢？~~）

标准序列容器：
1. [vector](./VectorLearning/Readme.md)
2. [string](./StringLearning/Readme.md)
3. [deque](./DequeLearning/Readme.md)
4. [list](./ListLearning/Readme.md)

标准关联容器：
1. [set](./SetLearning/Readme.md)
2. [multiset](./MultiSetLearning/Readme.md)
3. [map](./MapLearning/Readme.md)
4. [multimap](./MultiMapLearning/Readme.md)

非标准序列容器：
1. slist
2. rope

非标准关联容器：
1. hash_set
2. hash_multiset
3. hash_map
4. hash_multimap

非STL容器：
1. 数组(是默认的数组，不是array，array是动态数组，最好用vector来代替它)
2. bitset
3. valarray
4. stack
5. queue
6. priority_queue

## 关于STL的网站

SGI STL网站：www.sgi.com/tech/stl

----

注意：
所有的STL都不是线程安全的

如果想在多线程程序下安全使用STL那么要自己添加相关操作。
