﻿// BitsetsLearning.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <deque>

int main()
{
    std::cout << "Hello World!\n";
    int num = 10;

    std::deque<int> deque_;

    deque_.push_back(num);

    for (size_t i = 0; i < deque_.size(); i++) {
        std::cout << deque_[i] << " ";
    }
    std::cout << std::endl;
    
    return 0;
}
