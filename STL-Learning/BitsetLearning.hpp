#pragma once

#include "STLLearningHeaders.h"

int BitsetOfficalDemo()
{
    // constructors:
    constexpr std::bitset<4> b1;
    constexpr std::bitset<4> b2{ 0xA }; // == 0B1010
    std::bitset<4> b3{ "0011" }; // can't be constexpr yet
    std::bitset<8> b4{ "ABBA", /*length*/4, /*0:*/'A', /*1:*/'B' }; // == 0B0000'0110

    // bitsets can be printed out to a stream:
    std::cout << "b1:" << b1 << "; b2:" << b2 << "; b3:" << b3 << "; b4:" << b4 << '\n';

    // bitset supports bitwise operations:
    b3 |= 0b0100;
    //assert(b3 == 0b0111);
    std::cout << "b3 |= 0b0100;\t"<< b3 << std::endl;

    b3 &= 0b0011;
    //assert(b3 == 0b0011);
    std::cout << "b3 &= 0b0011;\t" << b3 << std::endl;

    b3 ^= std::bitset<4>{0b1100};
    //assert(b3 == 0b1111);
    std::cout << "b3 ^= std::bitset<4>{0b1100};\t" << b3 << std::endl;

    // operations on the whole set:
    b3.reset(); 
    //assert(b3 == 0);
    std::cout << "b3.reset(); \t" << b3 << std::endl;
    
    b3.set(); 
    //assert(b3 == 0b1111);
    std::cout << "b3.set();\t" << b3 << std::endl;

    //assert(b3.all() && b3.any() && !b3.none());
    b3.all() && b3.any() && !b3.none();
    std::cout << "b3.all() && b3.any() && !b3.none();\t" << b3 << std::endl;
    
    b3.flip(); //assert(b3 == 0);
    std::cout << "b3.flip();\t" << b3 << std::endl;

    // operations on individual bits:
    b3.set(/* position = */ 1, true); 
    //assert(b3 == 0b0010);
    std::cout << "b3.set(1, true); "<< b3 << std::endl;
    
    b3.set(/* position = */ 1, false); //assert(b3 == 0);
    std::cout << "b3.set(1, false); " << b3 << std::endl;
    
    b3.flip(/* position = */ 2); //assert(b3 == 0b0100);
    std::cout << "b3.flip(2)" << b3 << std::endl;
    
    b3.reset(/* position = */ 2); //assert(b3 == 0);
    std::cout << "b3.reset(2)" << b3 << std::endl;

    // subscript operator[] is supported:
    b3[2] = true; //assert(true == b3[2]);
    std::cout << "b3[2] is " << b3[2] << std::endl;

    // other operations:
    //assert(b3.count() == 1);
    std::cout << "b3.count() is " << b3.count() << std::endl;

    //assert(b3.size() == 4);
    std::cout << "b3.size() is " << b3.size() << std::endl;
    
    //assert(b3.to_ullong() == 0b0100ULL);
    std::cout << "b3.to_ullong() is" << b3.to_ullong() << std::endl;
    
    //assert(b3.to_string() == "0100");
    std::cout << "b3.to_string() is " << b3.to_string() << std::endl;

    return 0;
}

/// @brief 将两个bitset合并成一个
/// @param b1 被合并到左边的bitset（高位）
/// @param b2 被合并到右边的bitset（低位）
/// @return 
std::bitset<16> mergeBitset(std::bitset<8> b1, std::bitset<8> b2) {
    std::bitset<16> re;
    try
    {
        for (int i = 0; i < 8; i++)
        {
            re[i] = b2[i];
        }
        for (int i = 8; i < 16; i++)
        {
            re[i] = b1[i - 8];
        }
    }
    catch (const std::exception& err)
    {
        std::cout << err.what() << std::endl;
    }

    return re;
}

int BitsetMyDemo()
{
    std::cout << "Start processing bitsets!" << std::endl;

    std::bitset<8> b1{ "11110000" };
    std::bitset<8> b2{ "11110101" };

    std::cout << b1 << " " << b1.to_ulong() << std::endl;
    std::cout << b2 << " " << b2.to_ulong() << std::endl;

    std::bitset<16> re = mergeBitset(b1, b2);

    std::cout << re << " " << re.to_ulong() << std::endl;

    return 0;
}
