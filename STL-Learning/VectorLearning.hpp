#pragma once

#include "STLLearningHeaders.h"

namespace hrl {

	/// <summary>
	/// 这个类主要是用来学习vector的用法，元素类型为int
	/// </summary>
	class MyVector
	{
	public:
		MyVector();
		~MyVector();

		/* ----- 访问元素 ----- */

		/// <summary>
		/// 获取第一个元素
		/// </summary>
		/// <param name="e"></param>
		void getFirst(int& e) {
			e = this->data.front();
		}

		/// <summary>
		/// 获取最后一个元素
		/// </summary>
		/// <param name="e"></param>
		void getBack(int& e) {
			e = *(this->data.end() - 1);
			// 或者
			// e = this->data.back();
		}


		/// <summary>
		/// 获取指定位置的元素，并通过形参返回
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="e"></param>
		void getElement(int pos, int &e) {
			e = this->data.at(pos);
			// 或者
			// e = this->data[pos]
			// 但是这样效率高一丢丢，但是却没有越界检查，不够安全
		}

		/// <summary>
		/// 遍历所有元素并打印
		/// </summary>
		/// <returns></returns>
		bool traverseVector() {
			if (getSize() < 1)
				return false;

			for (auto i : data) {
				std::cout << i << " ";
			}
			std::cout << std::endl;

			return true;
		}

		/* ----- 对元素进行操作 ----- */

		/// <summary>
		/// 在vector最后面添加元素
		/// </summary>
		/// <param name="e"></param>
		void pushVector(int e) {
			this->data.push_back(e);
		}

		/// <summary>
		/// 在vector的最前面添加元素
		/// </summary>
		/// <param name="e"></param>
		void addFirst(int e) {
			// Todo
		}
		
		/// <summary>
		/// 删除vector最后一个元素，并通过形参返回
		/// </summary>
		/// <param name="e"></param>
		/// <details>
		/// 需要注意的是，删除元素只改变size，并不改变容量 capacity
		/// </details>
		void popVector(int &e) {
			e = this->data.back();
			this->data.pop_back();
		}

		/// <summary>
		/// 删除vector里第一个元素并通过形参返回
		/// </summary>
		/// <param name="e">被删除的元素的值</param>
		void deleteFirst(int &e) {
			try
			{
				e = *data.begin();
				this->data.erase(data.begin());
			}
			catch (const std::exception& err)
			{
				std::cout << err.what() << std::endl;
			}
		}

		//void deleteElement(int e) {
		//	this->data.erase(e);
		//}

		
		/* ----- 对整个vector进行操作 ----- */
		
		/// <summary>
		/// 清空vector里面的所有元素
		/// </summary>
		void clearVector() {
			this->data.clear();
		}

		/// <summary>
		/// 获取size
		/// </summary>
		/// <returns></returns>
		int getSize() {
			return this->data.size();
		}

		/// <summary>
		/// 判空
		/// </summary>
		/// <returns></returns>
		bool IsEmpty() {
			return this->data.empty();
		}

		/// <summary>
		/// 获取容量
		/// </summary>
		/// <returns></returns>
		int getCapacity() {
			return this->data.capacity();
		}

		// Todo
		// 
		// this->data.resize();
		// this->data.reserve();
		// this->data.assign();
		// this->data.cbegin();
		// this->data.cend();
		// this->data.crbegin();
		// this->data.crend();
		// this->data.emplace();
		// this->data.get_allocator();
		// this->data.max_size();
		// this->data.insert();
		// this->data.rbegin();
		// this->data.rend();
		// this->data.shrink_to_fit();
		// this->data._Emplace_reallocate();
		// this->data._Unchecked_begin();
		// this->data._Unchecked_end();

	private:
		std::vector<int> data;

	};

	MyVector::MyVector()
	{
	}

	MyVector::~MyVector()
	{
	}

}

void clear_learning() {
	std::vector<std::array<double, 3>> vec;
	vec.clear();
}
